<?php
/**
 * Created by PhpStorm.
 *
 * This Repository will load attendance data based on historic data
 *
 */

namespace App\Repositories;

use App\Models\Campus;
use App\Models\FlagType;
use App\Models\StructureEntry;
use App\Models\User;
use App\Models\UserEntry;
use App\Models\UserEntryComment;
use App\Models\Visitors;
use App\Models\VisitorsEntry;
use Illuminate\Support\Facades\Auth;

class LoadAttendanceRepository
{
    protected $user;
    protected $events;
    protected $memberEntry;

    public function __construct()
    {
        $this->user = Auth::User();
    }

    public function getMemberAttendanceData($structureEntryId)
    {
        $getMembers = $this->getReportMembers($structureEntryId);
        $getAttendance = $this->getAttendance($getMembers, $structureEntryId);
        $getServicesAcrossCampuses = $this->getServicesAcrossCampuses();

        return [
            'structure_entry_id' => $structureEntryId,
            'get_members' => $getMembers,
            'get_attendance' => $getAttendance,
            'service_details' => $getServicesAcrossCampuses,
        ];
    }

    public function getVisitorAttendanceData($structureEntryId)
    {
        $getMembers = $this->getReportMembers($structureEntryId);
        $getVisitors = $this->getReportVisitors($structureEntryId);
        $getAttendance = $this->getAttendance($getMembers, $structureEntryId);
        $getReportRevisits = $this->getReportRevisits($structureEntryId);


       return [
           'structure_entry_id' => $structureEntryId,
           'get_visitors' => $getVisitors,
           'get_attendance' => $getAttendance,
           'get_revisits' => $getReportRevisits,
       ];
    }

    public function getRevisitAttendanceData($structureEntryId)
    {
        $getMembers = $this->getReportMembers($structureEntryId);
        $getVisitors = $this->getReportVisitors($structureEntryId);
        $getAttendance = $this->getAttendance($getMembers, $structureEntryId);
        $getReportRevisits = $this->getReportRevisits($structureEntryId);
        $getServicesAcrossCampuses = $this->getServicesAcrossCampuses($structureEntryId);

        // TODO remove methods we don't need.

        return [
            'structure_entry_id' => $structureEntryId,
            'get_visitors' => $getVisitors,
            'get_members' => $getMembers,
            'get_attendance' => $getAttendance,
            'get_revisits' => $getReportRevisits,
            'service_details' => $getServicesAcrossCampuses,
        ];
    }

    public function getServicesAcrossCampuses()
    {
        $getServices = Campus::with('services')->get();
        return $getServices;
    }

    public function getListOfAttendanceReports()
    {
        // Display all reports for admin users
        if ($this->user->can('view_all_attendance_reports')) {
            $listAttendanceReports = StructureEntry::with('users', 'event')->groupBy(['event_type_id', 'id'])->orderBy('report_date', 'desc')->get();
        } else {
            $listAttendanceReports = StructureEntry::where('user_id', $this->user->id)->orderBy('report_date', 'desc')->get(['id']);
        }

        return [
            'list_attendance_reports' => $listAttendanceReports,
        ];
    }

    // Open an existing attendance report
    public function getReportMembers($structureEntryId)
    {

        $structureEntry = StructureEntry::with('campus')->where('id', $structureEntryId)->first();

        $parentId = $structureEntry->user_id;

        $getMembers = User::where('parent_id', $parentId)->get();


        foreach ($getMembers as $member) {
            $member['campus_name'] = $structureEntry['campus']->campus_name;
        }

        if ($getMembers) {
            return $getMembers;
        } else {
            return [];
        }
    }


    // Get list of visitors for the attendance report
    // Flag type - FTV

    public function getReportVisitors($structureEntryId)
    {

        $visitorFlag = FlagType::where('slug', 'visitor')->first();


        $visitorIds = VisitorsEntry::where('structure_entry_id', $structureEntryId)
            ->where('flag_type_id', $visitorFlag->id) // FTV flag
            ->pluck('visitor_id')
            ->toArray();

        if ($visitorIds) {
            // Get all users with the above user as it parent.
            $getVisitors = Visitors::whereIn('id', $visitorIds)
                ->get();

        }

        if ($getVisitors) {
            return $getVisitors;
        } else {
            return [];
        }
    }

    // Get list of Revisits for the attendance report
    public function getReportRevisits($structureEntryId)
    {

        $visitorFlag = FlagType::where('slug', 'revisit')->first();

        $visitorIds = VisitorsEntry::where('flag_type_id', $visitorFlag->id) // Revisits flag
            ->pluck('visitor_id')
            ->toArray();

        if ($visitorIds) {
            // Get all users with the above user as it parent.
            $getRevisits = Visitors::with(['entries' => function($query) use ($structureEntryId) {
                $query->where('structure_entry_id', $structureEntryId);
            }])->whereIn('id', $visitorIds)->get();
        }

        if ($getRevisits) {
            return $getRevisits;
        } else {
            return [];
        }
    }

    /**
     * @param $members
     * @param $structureEntryId
     * @return array|string
          example:
            3:  "absent"
            9: "present"
            10: "present"
            11: "present"
            12: "present"
            13: "absent"
     *
     *  [ member_id => slug ] i.e. 2 => present, 3 => absent
     */
    public function getAttendance($members, $structureEntryId)  // Only show present or Absent
    {
        $attendanceArray = [];
        $serviceAttendanceArray = [];
        $absentCommentArray = [];

        // Step through members
        foreach ($members as $member) {

            $checkServiceAttendance = UserEntry::with('service')
                ->where('user_id', $member->id)
                ->where('structure_entry_id', $structureEntryId)
                ->whereNotNull('service_id')->get();


            $getAbsentComments = UserEntry::with('comment')
                ->where('user_id', $member->id)
                ->where('structure_entry_id', $structureEntryId)
                ->get();

            foreach ($getAbsentComments as $comment) {

                if (isset($comment['comment'][0]['comment'])) {
                    $absentCommentArray[$member->id] = $comment['comment'][0]['comment'];
                }

            }

            if ($checkServiceAttendance) {

                $hasServices = false;

                foreach ($checkServiceAttendance as $attendance) {

                    $key = $attendance->user_id . '-' . $attendance->service_id;
                    $serviceAttendanceArray[$key] = true;

                    $hasServices = true;
                }

                if ($hasServices) {
                    $attendanceArray[$member->id] = [
                        'state' => 'present'
                    ];
                } else {
                    $attendanceArray[$member->id] = [
                        'state' => 'absent'
                    ];
                }

            }
            unset($checkServiceAttendance);
            unset($getAbsentComments);

        }

        return [
            'attendance' => $attendanceArray,
            'service_attendance' => $serviceAttendanceArray,
            'absent_comment' => $absentCommentArray,
        ];
    }


    /*
     *  $userType can be of type:  member/revisit/visitor/salvation
     *  This removeAttendanceType method removes the attendance on all services when
     *  i.e. present state changes to absent and user agrees to clear all attendances.
     */
    public function removeAttendanceType($structureEntryId, $userId, $userType)
    {


        if ($userType == 'members') {

            $getServices = UserEntry::with('service')
                ->whereNotNull('service_id')
                ->where('user_id', $userId)
                ->where('structure_entry_id', $structureEntryId)
                ->get(['id']);

            UserEntry::destroy($getServices->toArray());

            $status = "Removed all member services";

        }


        if ($userType == 'visitors') {

            $getServices = VisitorEntry::with('service')
                ->whereNotNull('service_id')
                ->where('user_id', $userId)
                ->where('structure_entry_id', $structureEntryId)
                ->get(['id']);

            VisitorEntry::destroy($getServices->toArray());

            $status = "Removed all visitor services";

        }

        // TODO - build out other types below
        // salvations
        // revisits
        // etc.


        return $status;

    }


    /**
     * @param mixed setMemberAbsentComment
     */
    public function setMemberAbsentComment($structureEntryId, $userId, $absentComment)
    {

        $absentCommentFlag = FlagType::where('slug', 'absent')->first();

        $newUserEntry = UserEntry::firstOrCreate([
            'structure_entry_id' => $structureEntryId,
            'user_id' => $userId,
            'flag_type_id' => $absentCommentFlag->id,
        ]);

        $setAbsentComment = UserEntryComment::firstOrCreate([
            'user_entry_id' => $newUserEntry->id
        ]);

        // Ensure only one entry is saved per user_entry_id at this stage.
        $setAbsentComment->comment = $absentComment;
        $setAbsentComment->save();

        return $setAbsentComment;

    }

    /*
     *  This setMemberAttendance() method works with the service checkbox, saving a new entry or deleting the entry that was already created based on the state of the checkbox. Checked or Unchecked.
     */
    public function setMemberAttendance($structureEntryId, $userId, $serviceId, $state)
    {

        $presentFlag = FlagType::where('slug', 'present')->first();
        $absentFlag  = FlagType::where('slug', 'absent')->first();

        $getAttendanceRecord = UserEntry::where(
            [
                'user_id' => $userId,
                'service_id' => $serviceId,
                'structure_entry_id' => $structureEntryId
            ])->whereIn('flag_type_id', [$absentFlag->id, $presentFlag->id])->first();

        // TODO resolve the state for save and delete
        if ($state === 'true') {

            if (!$getAttendanceRecord) {
                // If present then create entry

                UserEntry::create(
                    [
                        'user_id' => $userId,
                        'flag_type_id' => $presentFlag->id,
                        'service_id' => $serviceId,
                        'structure_entry_id' => $structureEntryId
                    ]);

                $status = 'Added';

            } else {
                $status = 'Already exist';
            }

        } else {

            if ($getAttendanceRecord) {
                $getAttendanceRecord->delete();
                $status = 'Deleted';
            } else {
                $status = 'Already deleted or not found';
            }

        }
        return $status;
    }

}
