<?php namespace App\Http\Controllers;

use App\Http\Requests\Request;
use Response;
use App\FinancesFilesMatching;
use App\FinancesFilesPossibleMatch;
use App\FinancesManuallyMatch;
use App\FinancesPaymentType;
use App\FinanceType;
use App\FinanceTypes;
use App\Http\Requests\CreateFinanceRequest;
use App\Finances;
use App\FinancesFile;
use App\FinancesFilesEntry;
use App\ReportTemplate;
use App\Service;
use App\User;
use App\Visitor;
use App\TransactionsLedger;
use App\MemberMaster;

class FinancesController extends Controller
{
    public function index()
    {
        $query = FinancesFile::whereNotNull('user_id')->with('service', 'user');
        if (\Input::has('match')) {
            $query->whereNull('matched_at');
            if (\Input::has('matched')) {
                $matched_file = FinancesFile::find(\Input::get('matched'));
            }
        } elseif (\Input::has('archive')) {
            $query->whereNotNull('matched_at');
            if (\Input::has('matched')) {
                $matched_file = FinancesFile::find(\Input::get('matched'));
            }
        } elseif (\Input::has('match_pending')) {
            $query->whereNull('matched_at');
            $query->where('new_pending_file', 'Y');
            if (\Input::has('matched')) {
                $matched_file = FinancesFile::find(\Input::get('matched'));
            }
        }
        $files = $query->orderBy('id', 'desc')->get();
        foreach ($files as &$file) {
            $file->entries = FinancesFilesEntry::where('finances_file_id', $file->id)->count();
            $file->matched = FinancesFilesEntry::where('finances_file_id', $file->id)
                ->Where(function ($query) {
                    $query->orwhere('user_id', '>', 0)->orWhere('visitor_id', '>', 0)->orWhere('anonymous', '=', 1)->orWhere('match_manually', '=', 1);
                })->count();
            $file->approved = FinancesFilesEntry::where('finances_file_id', $file->id)->whereNotNull('approved_at')->count();
        }
        if (\Input::has('archive')) {
            return view('admin.finances.archive', compact('files', 'matched_file'));
        } else {
            return view('admin.finances.index', compact('files', 'matched_file'));
        }
    }

    public function create()
    {
        $users = User::get()->lists('full_name', 'id');
        $finances_types = FinanceType::lists('name', 'id');
        $services = Service::lists('title', 'id');
        $activestep = \Input::get('activestep', 1);
        if (\Input::has('file_id') && $activestep == 1) {
            $entries = FinancesFilesEntry::where('finances_file_id', \Input::get('file_id'))->delete();
            FinancesFile::where('id', \Input::get('file_id'))->delete();
        } else if (\Input::has('file_id')) {
            $file = FinancesFile::find(\Input::get('file_id'));
            $entries = FinancesFilesEntry::with('user', 'finance_type', 'payment_type', 'finances_match')
                ->where('finances_file_id', \Input::get('file_id'))->get();
            $members = User::where('level_id', 10)->get()->lists('full_name', 'id');
        }
        return view('admin.finances.create', compact('finances_types', 'users', 'activestep', 'file', 'entries', 'members', 'services'));
    }

    public function store(CreateFinanceRequest $request)
    {
        if (\Input::has('file_id')) {
            if (\Input::has('members')) {
                foreach (\Input::get('members') as $entry_id => $member_id) {
                    if ($member_id != '') {
                        $entry = FinancesFilesEntry::find($entry_id);
                        $entry->user_id = $member_id;
                        $entry->save();
                    }
                }
            }
            return redirect(url('admin') . '#' . route('admin.finances.index'));
        } else {
            print_r($_FILES);
            $tmpName = $_FILES['finance_csv']['tmp_name'];
            $file = file_get_contents($tmpName);
            $finances_file = array_map("str_getcsv", preg_split('/\r*\n+|\r+/', $file));
            $finances_people = array();

            $allowed = array('.csv');
            $tmpName1 = $_FILES['finance_csv']['name'];
            $ext = substr($tmpName1, strpos($tmpName1, '.'), strlen($tmpName1) - 1);

            if (!in_array($ext, $allowed)) {
                return redirect(url('admin') . '#' . route('admin.finances.create'))->with('error', "Only CSV file allowed.");
            }
            if (!empty($finances_file)) {
                foreach ($finances_file as $member) {
                    if (array(null) !== $member && (isset($member[0]) && !empty($member[0])) && (isset($member[1]) && !empty($member[1])) && (isset($member[3]) && !empty($member[3]))) { // ignore blank lines
                        $finances_people[] = $member;
                    }
                }
            }

            foreach ($finances_people as $member) {

                if (count($member) > 9) {
                    return redirect(url('admin') . '#' . route('admin.finances.create'))->with('error', "The file has too many columns. Please rectify and upload again.");
                } else if (count($member) < 6) {
                    return redirect(url('admin') . '#' . route('admin.finances.create'))->with('error', "The file has too short columns. Please rectify and upload again.");
                } else if ($finances_people[0] && (strtolower($member[0]) == 'date' || strtolower($member[1]) == 'name' || strtolower($member[2]) == 'surname' || strtolower($member[3]) == 'amount' || strtolower($member[4]) == 'payment type' || strtolower($member[5]) == 'finances type')) {
                    return redirect(url('admin') . '#' . route('admin.finances.create'))->with('error', "The file first line has heading. Please remove heading from first line and upload again.");
                }
            }
            $file = new FinancesFile;
            $file->user_id = \Auth::user()->id;
            $file->filename = $_FILES['finance_csv']['name'];
            $file->event_date = \Input::get('event_date');
            $file->service_id = \Input::get('service_id');
            $file->save();
            foreach ($finances_people as $member) {
                $entry = new FinancesFilesEntry;
                $entry->finances_file_id = $file->id;
                $entry->finance_date = substr($member[0], 6, 4) . '-' . substr($member[0], 3, 2) . '-' . substr($member[0], 0, 2);
                $entry->name = $member[1];
                $entry->surname = $member[2];

                $entry->amount = $member[3];

                $payment_type = FinancesPaymentType::where('title', $member[4])->first();
                if (!$payment_type) {
                    $payment_type = new FinancesPaymentType;
                    $payment_type->title = $member[4];
                    $payment_type->save();
                }
                $entry->finances_payment_type_id = $payment_type->id;

                $finance_type = FinanceType::where('name', $member[5])->first();
                if (!$finance_type) {
                    $finance_type = new FinanceType;
                    $finance_type->name = $member[5];
                    $finance_type->save();
                }
                $entry->finances_type_id = $finance_type->id;

                $entry->giftaid = (isset($member[6]) && strtolower($member[6]) == 'yes') ? 1 : 0;
                $entry->phone = (isset($member[7])) ? $member[7] : '';
                $entry->postcode = (isset($member[8])) ? $member[8] : '';
                $entry->save();
            }
            return redirect(url('admin') . '#' . route('admin.finances.create') . '?file_id=' . $file->id . '&activestep=2');
        }
    }

    public function show($id)
    {
        $file = FinancesFile::find($id);
        $entries = FinancesFilesEntry::with('user', 'finance_type', 'payment_type', 'finances_match')
            ->where('finances_file_id', $id)->get();
        $total_amount = 0;
        foreach ($entries as $entry) $total_amount += $entry->amount;
        return view('admin.finances.show', compact('file', 'entries', 'total_amount'));
    }

    public function edit($id)
    {
        $file = FinancesFile::find($id);
        $entries = FinancesFilesEntry::with('user', 'finance_type', 'payment_type', 'finances_match')
            ->where('finances_file_id', $id)->get();
        return view('admin.finances.edit', compact('file', 'entries'));
    }

    public function update()
    {
        if (\Input::has('entries')) {
            foreach (\Input::get('entries') as $entry_id => $value) {
                $file_entry = FinancesFilesEntry::find($entry_id);
                $file_entry->approved_at = date('Y-m-d H:i:s');
                $file_entry->approved_by_id = \Auth::user()->id;
                $file_entry->save();
            }
        }
        return redirect(url('admin') . '#' . route('admin.finances.index'));
    }

    public function destroy($id)
    {
        ReportTemplate::destroy($id);

        return redirect(url('admin') . '#' . route('admin.reporttemplates.index'));
    }

    public function addEntries()
    {
        $users = User::get()->lists('full_name', 'id');
        $finances_types = FinanceType::lists('name', 'id');
        $payment_types = FinancesPaymentType::lists('title', 'id');
        $services = Service::lists('title', 'id');
        $activestep = \Input::get('activestep', 1);
        if (\Input::has('file_id')) {
            $file = FinancesFile::find(\Input::get('file_id'));
            $entries = FinancesFilesEntry::with('user', 'visitor', 'finance_type', 'payment_type', 'finances_match')
                ->where('finances_file_id', \Input::get('file_id'))->get();
            $members = User::where('level_id', 10)->get()->lists('full_name', 'id');
        }
        return view('admin.finances.add_entries', compact('finances_types', 'payment_types', 'users', 'activestep', 'file', 'entries', 'members', 'services'));
    }

    public function addEntriesStore()
    {
        $file = new FinancesFile;
        $file->user_id = \Auth::user()->id;
        $file->filename = \Input::get('filename');
        $file->event_date = \Input::get('event_date');
        $file->service_id = \Input::get('service_id');
        $file->save();
        return redirect(url('admin') . '#' . route('admin.finances.add_entries') . '?file_id=' . $file->id . '&activestep=2');
    }

    public function updateEntriesStore()
    {
        $file_id = \Input::get('finance_file_id');
        $file = FinancesFile::find($file_id);

        $entry = new FinancesFilesEntry;
        $entry->finances_file_id = $file_id;
        $entry->finance_date = $file->event_date;
        if (\Input::get('finances_members_chosen_type') == 'member') {
            $member_id = \Input::get('finances_members_chosen_id');
            $member = User::find($member_id);
            $entry->name = $member->name;
            $entry->surname = $member->surname;
            $entry->user_id = $member_id;
        } else {
            $visitor_id = \Input::get('finances_members_chosen_id');
            $visitor = Visitor::find($visitor_id);
            $entry->name = $visitor->name;
            $entry->surname = $visitor->surname;
            $entry->visitor_id = $visitor_id;
        }
        $entry->finance_date = \Input::get('finance_date');
        $entry->amount = \Input::get('amount');
        $entry->finances_payment_type_id = \Input::get('finances_payment_type_id');
        $entry->finances_type_id = \Input::get('finances_type_id');
        $entry->giftaid = (\Input::has('gift_aid')) ? 1 : 0;
        $entry->phone = '';
        $entry->postcode = '';
        $entry->save();

        return redirect(url('admin') . '#' . route('admin.finances.add_entries') . '?file_id=' . $file_id . '&activestep=2');
    }

    public function showMember($member)
    {
        return view('admin.finances.show_member', compact('member'));
    }


    public function exportMemberGiving()
    {
        return redirect('admin#admin/finances/members')->with('success', "CSV file exported successfully.");
    }


    public function getMatches($id)
    {
        $file = FinancesFile::find($id);
        $file->entries = FinancesFilesEntry::where('finances_file_id', $file->id)->count();
        $file->matched = FinancesFilesEntry::where('finances_file_id', $file->id)
            ->Where(function ($query) {
                $query->orwhere('user_id', '>', 0)->orWhere('visitor_id', '>', 0)->orWhere('anonymous', '=', 1)->orWhere('match_manually', '=', 1);
            })->count();

        $entries = FinancesFilesEntry::with('user', 'visitor', 'finance_type', 'payment_type', 'finances_match', 'possible_matches.user', 'possible_matches.visitor', 'finances_manually_match')->where('finances_file_id', $id)->get();
        return view('admin.finances.match', compact('file', 'entries'));
    }

    public function auto_match($id)
    {
        $matched = $this->performMatching($id);

        $file = FinancesFile::find($id);
        $file->entries = FinancesFilesEntry::where('finances_file_id', $file->id)->count();

        $file->matched = FinancesFilesEntry::where('finances_file_id', $file->id)
            ->Where(function ($query) {
                $query->orwhere('user_id', '>', 0)->orWhere('visitor_id', '>', 0)->orWhere('anonymous', '=', 1)->orWhere('match_manually', '=', 1);
            })->count();

        $entries = FinancesFilesEntry::with('user', 'visitor', 'finance_type', 'payment_type', 'finances_match', 'possible_matches.user', 'possible_matches.visitor', 'finances_manually_match')->where('finances_file_id', $id)->get();
        return view('admin.finances.match', compact('file', 'entries'));

    }

    public function performMatching($file_id)
    {
        $entries = FinancesFilesEntry::where('finances_file_id', $file_id)
            ->whereNull('user_id')->whereNull('visitor_id')
            ->orderBy('id')->skip(\Input::get('start', 0))->take(100)->get();
        if ($entries->count() == 0) return \Response::json(['processed' => 100, 'matched' => 0]);
        $matched = 0;
        foreach ($entries as $entry) {
            FinancesFilesPossibleMatch::where('finances_files_entry_id', $entry->id)->delete();
            if (!empty($entry->name) && !empty($entry->finance_date)) {

                $users = User::lookupUsers($entry->name, $entry->surname, $entry->phone, $entry->postcode);
                if ($users) {
                    $matched++;
                    foreach ($users as $user) {
                        FinancesFilesPossibleMatch::create([
                            'finances_files_entry_id' => $entry->id,
                            'finances_match_id' => $user['match_id'],
                            'user_id' => $user['user']->id,
                        ]);
                    }
                }
                $visitors = Visitor::lookupVisitors($entry->name, $entry->surname, $entry->phone, $entry->postcode);
                if ($visitors) {
                    $matched++;
                    foreach ($visitors as $visitor) {
                        FinancesFilesPossibleMatch::create([
                            'finances_files_entry_id' => $entry->id,
                            'finances_match_id' => $visitor['match_id'],
                            'visitor_id' => $visitor['visitor']->id,
                        ]);
                    }
                }
            }
        }
        return $matched;
    }

    public function finishMatching($id)
    {
        $file = FinancesFile::find($id);
        $file->matched_at = date('Y-m-d H:i:s');
        $file->save();

        $this->syncFinanceFileEntries($file);

        return redirect('admin/finances?match=1&matched=' . $id);
    }

    public function postJournalCSV($id)
    {
        if (empty($id)) {
            return redirect('admin/finances');
        }
        $entries = FinancesFilesEntry::with('finance_type', 'payment_type', 'finances_match', 'user', 'visitor')
            ->select('finances_payment_type_id', 'finances_type_id', 'finances_match_id', 'finance_date', 'user_id', 'visitor_id', 'anonymous', 'match_manually', 'name', 'surname', 'amount', 'giftaid', 'phone', 'postcode')->where('finances_file_id', $id)->get();

        $i = 0;
        $array = array();
        foreach ($entries as $row) {
            $array[$i]['Finance Date'] = $row->finance_date;
            $array[$i]['User ID'] = $row->user_id;
            $array[$i]['Visitor ID'] = $row->visitor_id;
            if ($row->anonymous == 1) {
                $Match_type = 'Anonymous';
            } else if ($row->match_manually == 1 || $row->finances_match->id == 9) {
                $Match_type = 'Match Manually';
            } else {
                $Match_type = '';
            }
            $array[$i]['Match Type'] = $Match_type;
            if ($row->finances_match->id == 9 && isset($row->user)) {
                $array[$i]['Name'] = $row->user->name;
                $array[$i]['Surname'] = $row->user->surname;
                $array[$i]['Giftaid'] = $row->user->giftaid;
            } else if ($row->finances_match->id == 9 && isset($row->visitor)) {
                $array[$i]['Name'] = $row->visitor->name;
                $array[$i]['Surname'] = $row->visitor->surname;
                $array[$i]['Giftaid'] = $row->visitor->giftaid;
            } else {
                $array[$i]['Name'] = $row->name;
                $array[$i]['Surname'] = $row->surname;
                $array[$i]['Giftaid'] = $row->giftaid;
            }
            $array[$i]['Amount'] = $row->amount;
            $array[$i]['Payment Type'] = $row->payment_type->title;
  
            if ($row->finances_match->id == 9 && isset($row->user)) {
                $array[$i]['Phone'] = $row->user->phone;
                $array[$i]['Postcode'] = $row->user->postcode;
            } else if ($row->finances_match->id == 9 && isset($row->visitor)) {
                $array[$i]['Phone'] = $row->visitor->mobile;
                $array[$i]['Postcode'] = $row->visitor->postcode;
            } else {
                $array[$i]['Phone'] = $row->phone;
                $array[$i]['Postcode'] = $row->postcode;
            }

            $array[$i]['Finances Match'] = $row->finances_match->title;
            $i++;
        }
        $file_name = "post_journal" . time() . ".csv";
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $file_name);
        $output = fopen('php://output', 'w');
        fputcsv($output, array('Finance Date', 'User ID', 'Visitor ID', 'Match Type', 'Name', 'Surname', 'Amount', 'Payment Type', 'Giftaid', 'Phone', 'Postcode', 'Finances Match'));
        foreach ($array as $key => $value) {
            fputcsv($output, $value);
        }
        fclose($output);
        exit;
    }

    public function saveMatching()
    {
        $file_id = \Input::get('file_id');
        $Is_Post_Journal = \Input::get('btn_match_finish');
        $matches = \Input::get('matches', []);
        $match_later_id = array();
     
        if (isset($Is_Post_Journal)) {
            foreach ($matches as $entry_id => $match_id) {
                if ($match_id == -3) {
                    $match_later_id[] = $entry_id;
                }
            }
            $match_later_file = FinancesFile::find($file_id);

            if (!empty($match_later_id) && !empty($match_later_file->id)) {
                $file = new FinancesFile;
                $file->user_id = \Auth::user()->id;
                $file->filename = basename($match_later_file->filename, ".csv") . "_pending_entries.csv";
                $file->event_date = $match_later_file->event_date;
                $file->service_id = $match_later_file->service_id;
                $file->new_pending_file = 'Y';
                $file->save();
                // print_r($match_later); exit;
                foreach ($match_later_id as $match_later) {
                    FinancesFilesEntry::where('id', $match_later)
                        ->update(['parent_finances_file_id' => $file_id, 'finances_file_id' => $file->id, 'user_id' => NULL, 'visitor_id' => NULL, 'match_manually' => NULL, 'anonymous' => NULL, 'finances_match_id' => NULL]);
                }
            }

            $file = FinancesFile::find($file_id);
            $file->matched_at = date('Y-m-d H:i:s');
            $file->save();
            return redirect('admin#admin/finances?match=1&matched=' . $file_id);

        } else {
            foreach ($matches as $entry_id => $match_id) {
                $entry = FinancesFilesEntry::find($entry_id);
                if ($match_id != "") {
                    if ($match_id == -1) {
                        $entry->user_id = NULL;
                        $entry->visitor_id = NULL;
                        $entry->match_manually = NULL;
                        $entry->anonymous = 1;
                        $entry->finances_match_id = 8;
                        $entry->match_later = NULL;
                    } else if ($match_id == -2) {
                        $entry->user_id = NULL;
                        $entry->visitor_id = NULL;
                        $entry->anonymous = NULL;
                        $entry->match_manually = 1;
                        $entry->finances_match_id = 9;
                        $entry->match_later = NULL;

                        $match_manually = FinancesManuallyMatch::findOrCreate($entry_id);
                        $match_manually->finances_files_entries_id = $entry_id;
                        $match_manually->match_by = \Input::get('match_manually_by_' . $entry_id);
                        $match_manually->match_val = \Input::get('match_manually_val_' . $entry_id);
                        $match_manually->save();

                        FinancesFilesPossibleMatch::where('finances_files_entry_id', $entry_id)->delete();
                        $name = '';
                        $surname = '';
                        $phone = '';
                        $postcode = '';
                        $match = false;
                        if ($match_manually->match_by == 1) {
                            $name = $match_manually->match_val;
                            $match = true;
                        } else if ($match_manually->match_by == 2) {
                            $surname = $match_manually->match_val;
                            $match = true;
                        } else if ($match_manually->match_by == 3) {
                            $phone = $match_manually->match_val;
                            $match = true;
                        } else if ($match_manually->match_by == 4) {
                            $postcode = $match_manually->match_val;
                            $match = true;
                        }
                        if ($match == true) {
                            $users = User::lookupUsers($name, $surname, $phone, $postcode);
                            if ($users) {
                                foreach ($users as $user) {
                                    FinancesFilesPossibleMatch::create([
                                        'finances_files_entry_id' => $entry_id,
                                        'finances_match_id' => $user['match_id'],
                                        'user_id' => $user['user']->id,
                                    ]);
                                }
                            }
                            $visitors = Visitor::lookupVisitors($name, $surname, $phone, $postcode);
                            if ($visitors) {
                                foreach ($visitors as $visitor) {
                                    FinancesFilesPossibleMatch::create([
                                        'finances_files_entry_id' => $entry_id,
                                        'finances_match_id' => $visitor['match_id'],
                                        'visitor_id' => $visitor['visitor']->id,
                                    ]);
                                }
                            }
                        }
                    } else if ($match_id == -3) {
                        //$match_later_id[] = $entry_id;
                        $entry->user_id = NULL;
                        $entry->visitor_id = NULL;
                        $entry->match_manually = NULL;
                        $entry->anonymous = NULL;
                        $entry->match_later = 1;
                        $entry->finances_match_id = NULL;
                    } else {
                        $entry->match_later = NULL;
                        $entry->anonymous = NULL;
                        $match = FinancesFilesPossibleMatch::find($match_id);

                        $entry->match_manually = NULL;
                        $entry->user_id = $match->user_id;
                        $entry->visitor_id = $match->visitor_id;
                        if ($entry->finances_match_id != 9) {
                            $entry->finances_match_id = $match->finances_match_id;
                        }
                    }
                } else {
                    $entry->anonymous = '';
                    $entry->user_id = '';
                    $data = [
                        'name' => $entry->name,
                        'surname' => $entry->surname,
                        'mobile' => $entry->phone,
                        'postcode' => $entry->postcode
                    ];
                    $visitor = Visitor::create($data);

                    $entry->visitor_id = $visitor->id;
                    $entry->finances_match_id = 7;
                }
                $entry->save();
                if ($match_id != -2) {
                    FinancesManuallyMatch::where('finances_files_entries_id', $entry_id)->delete();
                }
            }

            return redirect('admin#admin/finances/match/' . $file_id);
        }
    }

    public function postMatchEntry()
    {
        $entry = FinancesFilesEntry::find(\Input::get('entry_id', 0));
        if ($entry) {
            $user = User::lookupUser($entry->name, $entry->surname, $entry->phone, $entry->postcode);
            if ($user) {
                $entry->finances_match_id = $user['match_id'];
                $entry->user_id = $user['user']->id;
                $entry->save();
            }
        }
        return \Response::json(json_encode(['result' => 'success']));
    }

    public function rollBack()
    {
        $file_ids = \Input::get('file_ids');
        if (!empty($file_ids)) {
            $ids = explode(",", $file_ids);
            foreach ($ids as $file_id) {
                $file = FinancesFile::find($file_id);
                $file->matched_at = NULL;
                $file->save();
   
                FinancesFilesEntry::where('finances_file_id', $file_id)
                    ->update(['user_id' => NULL, 'visitor_id' => NULL, 'match_manually' => NULL, 'anonymous' => NULL, 'finances_match_id' => NULL]);

            }
        }
        return redirect(url('admin') . '#' . route('admin.finances.index') . "?archive=1")->with('success', "Your selected file(s) has been successfully roll backed. ");
    }

    public function syncFinances()
    {
        $finances_files = FinancesFile::where('matched_at', '>', '2018-01-01')
            ->whereNull('synched_at')
            //->where('updated_at','>','2018-01-31')
            //->whereNull('synched_at')
            ->get();

        if (count($finances_files) > 0) {
            foreach ($finances_files as $finances_file) {
                $update = $this->synch_financefile_entries($finances_file->id);
            }

            return redirect('admin#admin/finances?archive=1')->with('success', "Your selected file(s) has been successfully synched to CRC Admin. ");

        } else {
            return redirect('admin#admin/finances?archive=1')->with('success', "All files already synched to CRC Admin. ");

        }
    }

    public function syncFinanceFileEntries($finances_file_id)
    {

        $finances_file = FinancesFile::find($finances_file_id);
        if ($finances_file->id <> '') {
            $finances_file_entries = FinancesFilesEntry::where('Finances_file_id', $finances_file->id)->get();
            if (count($finances_file_entries) > 0) {
                foreach ($finances_file_entries as $entry) {
                    if ($entry->user_id <> '') {
                        $member_id = $this->getMemberMasterId($entry->user_id);

                        $giftaid_status = $this->getGiftAidStatus($entry->user_id);

                        if ($entry->visitor_id == '') {
                            $entry->visitor_id = 0;
                        }

                        if ($entry->finances_type_id == '') {
                            $type_id = 0;
                        } else {
                            $type_id = $this->getOldTypeId($entry->finances_type_id);
                        }

                        $finances = Finances::where('finances_files_entry_id', $entry->id)
                            ->where('finances_file_id', $entry->finances_file_id)
                            ->get();
                        if (count($finances) <= 0) {
                            $finances_id = Finances::insertGetId(['created_at' => date('Y-m-d H:i:s'), 'payment_date' => $entry->finance_date, 'member_id' => $member_id, 'visitor_id' => $entry->visitor_id, 'type_id' => $type_id, 'payment_type_id' => $entry->finances_payment_type_id, 'amount' => $entry->amount, 'gift_aid' => $giftaid_status, 'approved' => $finances_file->user_id, 'finances_files_entry_id' => $entry->id, 'finances_file_id' => $entry->finances_file_id]);
                        }
                    } elseif ($entry->visitor_id <> '') {
                        $member_id = 0;

                        $giftaid_status = $this->getGiftAidStatus($entry->user_id);

                        if ($entry->finances_type_id == '') {
                            $type_id = 0;
                        } else {
                            $type_id = $this->getOldTypeId($entry->finances_type_id);
                        }

                        if ($entry->visitor_id == '') {
                            $entry->visitor_id = 0;
                        }
                        if ($entry->finances_type_id == '') {
                            $entry->finances_type_id = 0;
                        }

                        $finances = Finances::where('finances_files_entry_id', $entry->id)
                            ->where('finances_file_id', $entry->finances_file_id)
                            ->get();
                        if (count($finances) <= 0) {
                            $finances_id = Finances::insertGetId(['created_at' => date('Y-m-d H:i:s'), 'payment_date' => $entry->finance_date, 'member_id' => $member_id, 'visitor_id' => $entry->visitor_id, 'type_id' => $type_id, 'payment_type_id' => $entry->finances_payment_type_id, 'amount' => $entry->amount, 'gift_aid' => $giftaid_status, 'approved' => $finances_file->user_id, 'finances_files_entry_id' => $entry->id, 'finances_file_id' => $entry->finances_file_id]);
                        }
                    }
                }

                $finances_file->synched_at = date('Y-m-d H:i:s');
                $finances_file->save();
            }
        }

        return $finances_file_id;
    }

    public function getMemberMasterId($user_id)
    {
        $member_id = 0;
        if ($user_id <> '') {
            $users = User::where('id', $user_id)->get();
            foreach ($users as $user) {
                $member_id = $user->old_member_master_id;
            }
        }
        return $member_id;
    }
    
    public function getGiftAidStatus($user_id)
    {
        $status = 0;
        $users = User::where('id', $user_id)->get();
        foreach ($users as $user) {
            $entry = Finances::where('member_id', $user->old_member_master_id)
                ->orderBy('created_at', 'asc')
                ->get();
            if (count($entry) > 0) {
                foreach ($entry as $finance_entry)
                    $status = $finance_entry->gift_aid;
            }
        }
        return $status;
    }

    public function getMembers()
    {
        if (\Input::has('keyword')) {
            $query = User::where('branch_id', \Auth::user()->branch_id);
            $query->where(function ($q) {
                $q->where('name', 'like', '%' . \Input::get('keyword') . '%')
                    ->orWhere('surname', 'like', '%' . \Input::get('keyword') . '%');
                return $q;
            });
            $members = $query->get();
        }
        return view('admin.finances.members', compact('members'));
    }

    public function matchSuggestion()
    {

        $result = User::skip(10)->take(5)->get();

        foreach ($result as $row) {
            $user_arr2[] = $row->name;
        }

        $results = array();
        $term = \Input::get('term');
        $manual_val = \Input::get('manual_val');
        if ($manual_val == 2) {
            $query = \DB::table('users')->select('id', 'surname')->where('surname', 'LIKE', '%' . $term . '%')->groupBy('surname')->take(20);
            $query1 = \DB::table('visitors')->select('id', 'surname')->where('surname', 'LIKE', '%' . $term . '%')->groupBy('surname')->take(20);
            $users = $query1->union($query)->groupBy('surname')->orderBy('surname', 'asc')->get();

            foreach ($users as $row) {
                $results[] = ['id' => $row->surname, 'value' => $row->surname];
            }
        } else if ($manual_val == 3) {
            $query = \DB::table('users')->select('id', 'phone')->where('phone', 'LIKE', '%' . $term . '%')->groupBy('phone')->take(20);
            $query1 = \DB::table('visitors')->select('id', 'mobile as phone')->where('mobile', 'LIKE', '%' . $term . '%')->groupBy('mobile')->take(20);
            $users = $query1->union($query)->groupBy('phone')->orderBy('phone', 'asc')->get();

            foreach ($users as $row) {
                $results[] = ['id' => $row->phone, 'value' => $row->phone];
            }
        } else if ($manual_val == 4) {
            $query = \DB::table('users')->select('id', 'postcode')->where('postcode', 'LIKE', '%' . $term . '%')->groupBy('postcode')->take(20);
            $query1 = \DB::table('visitors')->select('id', 'postcode')->where('postcode', 'LIKE', '%' . $term . '%')->groupBy('postcode')->take(20);
            $users = $query1->union($query)->groupBy('postcode')->orderBy('postcode', 'asc')->get();

            foreach ($users as $row) {
                $results[] = ['id' => $row->postcode, 'value' => $row->postcode];
            }
        } else {
            $query = \DB::table('users')->select('id', 'name')->where('name', 'LIKE', '%' . $term . '%')->groupBy('name')->take(20);
            $query1 = \DB::table('visitors')->select('id', 'name')->where('name', 'LIKE', '%' . $term . '%')->groupBy('name')->take(20);
            $users = $query1->union($query)->groupBy('name')->orderBy('name', 'asc')->get();

            foreach ($users as $row) {
                $results[] = ['id' => $row->name, 'value' => $row->name];
            }
        }

        return Response::json($results);
    }

    public function transactionLedger()
    {
        $transactions = TransactionsLedger::where('date', '>', '2016-09-01')->get();
        return view('admin/finances/transactionsledger', compact('transactions'));
    }

    public function transactionsEdit($entry_id)
    {
        $transaction = Finances::find($entry_id);

        $members = MemberMaster::where('id', $transaction->member_id)->get();
        $giving_type = FinanceTypes::find($transaction->type_id);
        foreach ($members as $member) {
            return view('admin/finances/transactionedit', compact('transaction', 'member', 'giving_type'));
        }

    }

    public function transactionsUpdate(Request $request, $entry_id)
    {
        $gift_aid = $request->input('gift_aid');
        $type_id = $request->input('type_id');
        $amount = $request->input('amount');
        $transactions = TransactionsLedger::where('id', $entry_id)->get();
        
        foreach ($transactions as $transaction) {
            $transaction->gift_aid = $gift_aid;
            $transaction->type_id = $type_id;
            $transaction->amount = $amount;
            $transaction->save();
        }
        return view('admin/finances/transactionsledger', compact('transactions'));
    }


}
