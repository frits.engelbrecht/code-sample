<?php namespace App;

use App\Http\Controllers\Traits\UserPermissionsTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Hash;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword, UserPermissionsTrait, SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'city_id',
        'branch_id',
        'level_id',
        'parent_id',
        'name',
        'email',
        'password',
        'role',
        'surname',
        'phone',
        'married',
        'minor',
        'address_1',
        'address_2',
        'county',
        'city',
        'postcode',
        'gender',
        'date_of_birth',
        'active',
        'old_member_master_id',
        'logged_at',
        'homegroup_id',
        'goal_salvations',
        'goal_visitors',
        'goal_new_members',
        'goal_membership',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes appended.
     *
     * @var array
     */
    protected $appends = ['full_name', 'donations'];

    /**
     * Always hash password when storing it.
     *
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Always hash password when storing it.
     *
     * @param $value
     */
    public function getFullNameAttribute($value)
    {
        return $this->name . ' ' . $this->surname;
    }

    public function level()
    {
        return $this->belongsTo('App\Level');
    }

    public function parent()
    {
        return $this->belongsTo('App\User', 'parent_id');
    }

    public function ministries()
    {
        return $this->hasMany('App\UserMinistry', 'user_id');
    }

    /**
     * Get breadcrumbs
     *
     * @param $user_id
     */
    public static function getBreadcrumbs($user_id)
    {
        $level = 0;
        $breadcrumbs = '';
        do {
            $user = User::find($user_id);
            if ($user) {
                $breadcrumbs = '<a href="#' . route('admin.users.list', $user->id) . '">' . $user->name . ' ' . $user->surname . '</a> > ' . $breadcrumbs;
                $user_id = $user->parent_id;
            }
            $level++;
        } while ($user && $level <= 10);
        if ($level <= 2) $breadcrumbs = '';
        $breadcrumbs = substr($breadcrumbs, 0, -2);
        return $breadcrumbs;
    }

    public static function getEvents($user_id)
    {
        $events = [];
        $entries = ReportEntry::with('entry_type')->where('user_id', $user_id)->orderBy('id', 'desc')->get();
        foreach ($entries as $entry) {
            $events[] = [
                'created_at' => $entry->created_at->toDateTimeString(),
                'event' => 'Members Attendance report: ' . $entry->entry_type->name
            ];
        }
        return $events;
    }

    public static function lookupUser($name, $surname, $phone, $postcode)
    {
        $users = User::where('name', $name)->where('surname', $surname)->get();
        if ($users) {
            if ($users->count() == 1) return ['match_id' => 1, 'user' => $users[0]];
            foreach ($users as $user) {
                if ($user->phone == $phone) return ['match_id' => 1, 'user' => $user];
                if ($user->postcode == $postcode) return ['match_id' => 1, 'user' => $user];
            }
        }

        $user = User::where('phone', $phone)->first();
        if ($user) return ['match_id' => 2, 'user' => $user];

        $user = User::where('postcode', $postcode)->first();
        if ($user) return ['match_id' => 3, 'user' => $user];

        return FALSE;
    }

    public static function lookupUsers($name, $surname, $phone, $postcode)
    {
        $users = User::whereIn('level_id', [10])
            ->where(function ($query) use ($name, $surname, $phone, $postcode) {
                if (trim($name) != '') $query->where('name', $name);
                if (trim($surname) != '') $query->orWhere('surname', $surname);
                if (trim($phone) != '') $query->orWhere('phone', $phone);
                if (trim($postcode) != '') $query->orWhere('postcode', $postcode);
            })->take(100)->get();
        $matches = [];
        if ($users) {
            foreach ($users as $user) {
                if (strtolower($user->name) == strtolower($name) && strtolower($user->surname) == strtolower($surname)) {
                    $matches[] = ['match_id' => 1, 'user' => $user, 'position' => 1];
                } else if (trim(strtolower($user->name)) == trim(strtolower($name))) {
                    $matches[] = ['match_id' => 5, 'user' => $user, 'position' => 3];
                } else if (trim(strtolower($user->surname)) == trim(strtolower($surname))) {
                    $matches[] = ['match_id' => 6, 'user' => $user, 'position' => 2];
                } else if ($user->phone == $phone) {
                    $matches[] = ['match_id' => 2, 'user' => $user, 'position' => 4];
                } else if (strtolower($user->postcode) == strtolower($postcode)) {
                    $matches[] = ['match_id' => 3, 'user' => $user, 'position' => 5];
                }
            }
        }
        for ($i = 0; $i < count($matches) - 1; $i++) {
            for ($j = 0; $j < count($matches) - 1; $j++) {
                if ($matches[$j]['position'] > $matches[$j + 1]['position']) {
                    $t = $matches[$j];
                    $matches[$j] = $matches[$j + 1];
                    $matches[$j + 1] = $t;
                }
            }
        }
        return $matches;
    }

    public function getDonationsAttribute()
    {
        $donations = FinancesFilesEntry::select('finances_files_entries.*')
            ->join('finances_files', 'finances_files_entries.finances_file_id', '=', 'finances_files.id')
            ->where('finances_files_entries.user_id', $this->attributes['id'])
            ->whereNotNull('finances_files.matched_at')
            ->orderBy('finances_files_entries.finance_date', 'desc')
            ->get();
        if (!$donations) return ['donations' => [], 'sum_amount' => 0];
        $result = ['donations' => [], 'sum_amount' => 0];
        foreach ($donations as $donation) {
            $result['donations'][] = $donation;
            $result['sum_amount'] += $donation->amount;
        }
        return $result;
    }

    public static function getLogins()
    {
        $logins = User::where('level_id', '<=', 9)
            ->orderBy('level_id')
            ->get();
        return $logins;
    }

    public static function getParentsForMembers($level_id)
    {
        $parent_level_id = \Auth::user()->level_id;
        $level = Level::find($level_id);
        $parents = [\Auth::user()->id];
        do {
            $parent_level_id = Level::where('parent_id', $parent_level_id)->pluck('id');
            if ($parent_level_id <= $level->parent_id)
                $parents = User::whereIn('parent_id', $parents)->lists('id');
        } while ($parent_level_id < $level->parent_id);
        return $parents;
    }

    public static function getMembersList($level_id, $parent_id = null)
    {
        $query = User::with('ministries.ministry')
            // ->where('branch_id', \Auth::user()->branch_id)
            ->where('level_id', '=', $level_id)
            ->where('active', 1);

        if ($parent_id && $parent_id != '') $query->where('parent_id', '=', $parent_id);

        if ($level_id == 10 && \Auth::user()->level_id == 9) $query->where('parent_id', '=', \Auth::user()->id);

        $parents = Static::getParentsForMembers($level_id);
        $query->whereIn('parent_id', $parents);

        $members_rows = $query->get();
        $members = [];
        foreach ($members_rows as $member) {
            if (isset($member->ministries) && $member->ministries->count() > 0) {
                $ministries = [];
                foreach ($member->ministries as $ministry) {
                    $ministries[] = $ministry->ministry->title;
                }
                $member->ministries = implode('<br />', $ministries);
            } else {
                $member->ministries = '';
            }

            if ($member->level_id < 10) {

                $member->salvations = GraphPerLevel::where('user_id', $member->id)
                    ->where('date', '>', '2017-09-01')
                    ->sum('salvations');

                $member->visitors = GraphPerLevel::where('user_id', $member->id)
                    ->where('date', '>', '2017-09-01')
                    ->sum('visitors');

                $member->members_added = GraphPerLevel::where('user_id', $member->id)
                    ->where('date', '>', '2017-09-01')
                    ->sum('members_added');

                $member->members_deleted = GraphPerLevel::where('user_id', $member->id)
                    ->where('date', '>', '2017-09-01')
                    ->sum('members_deleted');

                $member->growth = GraphPerLevel::where('user_id', $member->id)
                    ->where('date', '>', '2017-09-01')
                    ->sum('growth');

                $member->avg_att_adults = GraphPerLevel::where('user_id', $member->id)
                    ->where('date', '>', '2017-09-01')
                    ->avg('percentage_adults');

                $member->avg_att_youth = GraphPerLevel::where('user_id', $member->id)
                    ->where('date', '>', '2017-09-01')
                    ->avg('percentage_youth');

                $member->avg_att_children = GraphPerLevel::where('user_id', $member->id)
                    ->where('date', '>', '2017-09-01')
                    ->avg('percentage_children');

                $member->avg_att_overall = GraphPerLevel::where('user_id', $member->id)
                    ->where('date', '>', '2017-09-01')
                    ->avg('percentage_overall');

                $graphperlevel = GraphPerLevel::where('user_id', $member->id)
                    ->where('date', '>', '2017-09-01')
                    ->orderBy('date', 'desc')
                    ->limit(1)
                    ->get();

                foreach ($graphperlevel as $stats) {
                    $member->membership = $stats->membership;
                    $member->stats = $stats;
                    $member->adults = $stats->adults;
                    $member->youth = $stats->youth;
                    $member->kids = $stats->kids;
                    $member->percentage_overall = $stats->percentage_overall;
                }
            }

            $members[] = $member;
        }
        return ['members' => $members, 'parents' => $parents];
    }

}
