<?php namespace App\Http\Controllers;

use App\Lifegroup;
use App\User;
use App\Level;
use Auth;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Requests\CreateLifegroupRequest;
use App\Http\Requests\EditLifegroupRequest;

class LifegroupsController extends Controller
{

    /**
     * @param int $user_id
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $parents = User::getParentsForMembers(9);

        $lifegroups = Lifegroup::select('lifegroups.*')
            ->join('users', 'lifegroups.leader_id', '=', 'users.id')
            ->where('users.branch_id', \Auth::user()->branch_id)
            ->whereIn('users.parent_id', $parents)
            ->with('leader.level')->get();
        return view('admin.lifegroups.index', compact('lifegroups'));
    }


    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $levels = Level::getAllLevels(\Auth::user()->level_id);
        $members = User::where('branch_id', \Auth::user()->branch_id)
            ->whereIn('level_id', $levels)
            ->orderBy('surname')
            ->orderBy('name')
            ->get()
            ->lists('full_name', 'id');
        return view('admin.lifegroups.create', compact('members'));
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function store(CreateLifegroupRequest $request)
    {
        Lifegroup::create($request->all());

        $leader = User::find($request->get('leader_id'));
        $supervisor = User::find($request->get('supervisor_id'));
        $leader->parent_id = $supervisor->id;
        $level = Level::where('parent_id', $supervisor->level_id)->first();
        $leader->level_id = $level->id;
        $leader->save();

        return redirect(url('admin') . '#' . url('admin/lifegroups'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $users = User::findOrFail($id);

        return view('admin.users.show', compact('users'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $lifegroups = Lifegroup::with('leader.level')->find($id);

        return view('admin.lifegroups.edit', compact('lifegroups'));
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return mixed
     */
    public function update($id, EditLifegroupRequest $request)
    {

        $lifegroup = Lifegroup::findOrFail($id);

        $lifegroup->update($request->all());

        return redirect(url('admin') . '#' . route('admin.lifegroups.index'));
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function destroy($id)
    {

        User::destroy($id);

        return redirect(url('admin') . '#' . route('admin.users.index'));
    }
}