import {Component, OnInit} from '@angular/core';
import {AttendanceService} from '../../_services/index';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    templateUrl: 'member-attendance.component.html'
})

export class MemberAttendanceComponent implements OnInit
    {

    protected allServices: any = [];
    protected initCampuses: any = [];
    protected memberArray: any = [];
    protected campusArray: any = [];
    protected serviceArray: any = [];
    protected attendanceData: any = [];
    protected memberActiveServices: any = [];
    protected savedAttendanceArray: any = [];
    protected absentCommentArray: any = [];
    protected attendanceReportId: number;

    constructor(private attendanceService: AttendanceService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit()
    {
        this.attendanceReportId = Number(this.route.snapshot.paramMap.get('id'));
        this.loadReport();
    }

    updateMemberAttendance(state, currentstate, userId)
    {

        state = state || 'present';
        currentstate = currentstate || 'present';

        if (state != currentstate) {  // If present/absent state changed.

            this.savedAttendanceArray[userId].state = state;

            if (state == 'absent') {
                // Remove from API
                this.attendanceService.removeServiceAttendanceForUserType(userId, this.attendanceData['structure_entry_id'], 'members').subscribe(
                    res => res,
                    (err) => console.log(err),
                    () => this.clearAllUserServicesFromCache(userId)
                );
            }
        }
    }

    loadReport()
    {

        if (this.attendanceReportId !== 0) {
            if (this.attendanceReportId) {
                this.attendanceService.memberAttendanceInfo(this.attendanceReportId).subscribe(
                    res => this.attendanceData = res,
                    (err) => console.log(err),
                    () => this.saveAttendanceArray()
                );
            }
        } else {
            // if URL is malformed, redirect to home
            this.router.navigate(['/']);
        }
    }

    saveAttendanceArray()
    {
        this.memberActiveServices = this.attendanceData['get_attendance']['service_attendance'] || [];
        this.savedAttendanceArray = this.attendanceData['get_attendance']['attendance'] || [];
        this.memberArray = this.attendanceData['get_members'] || [];
        this.absentCommentArray = this.attendanceData['get_attendance']['absent_comment'];

        this.createArrayOfAllServices();
        this.assignDefaultServicesToMembers();
    }


    assignDefaultServicesToMembers()
    {
        // Update initial campus and services details, based on defaults set on database.
        for (let x = 0; x < this.memberArray.length; x++) {
            this.serviceArray[this.memberArray[x].id] = this.initCampuses[this.memberArray[x].campus_name];
        }

    }

    updateCampus(campusName, memberIndex = 0)
    {

        if (memberIndex) {
            this.serviceArray[memberIndex] = this.initCampuses[campusName];
            return;
        }
    }

    createArrayOfAllServices()
    {

        /*
            Return two important variables.
                this.allServices => [1,2,3....] ids of all services, used to clear all attendances.
                this.initCampus => ['Campus 1' => Campus 1 service details, 'Campus 2' => Campus 2 service details etc.]
         */

        this.allServices = [];
        this.initCampuses = [];

        for (let x = 0; x < this.attendanceData.service_details.length; x++) {

            let services = this.attendanceData.service_details[x].services;
            let campusName = this.attendanceData.service_details[x].campus_name;

            this.initCampuses[campusName] = this.attendanceData.service_details[x].services;

            for (let i = 0; i < services.length; i++) {
                this.allServices.push(services[i]['id']);
            }
        }
    }

    // Used to clear a user of all service attendance

    clearAllUserServicesFromCache(userId)
    {

        if (typeof this.allServices === 'undefined') {
            this.createArrayOfAllServices(); // If null or notRe-initialize allServices
        }

        // Compile the keys with userId and serviceId keys.
        /* i.e. userId + '-' + serviceId, 5-3: true */

        let generateKeys = [];

        for (let x = 0; x < this.allServices.length; x++) {
            let serviceId = this.allServices[x];
            generateKeys.push(userId + '-' + serviceId);
        }

        // Now reset the values for these keys to false.
        for (let i = 0; i < generateKeys.length; i++) {
            this.memberActiveServices[generateKeys[i]] = false;
        }
    }

    updateAbsentComment(userId) {
        this.attendanceService.updateUserAbsentComment(userId, this.attendanceData['structure_entry_id'], this.absentCommentArray[userId])
            .subscribe((res) => res);
    }

    // Handle the logic to save member services.

    updateService(serviceId, userId) {

        // Checkbox state. true or false.
        const state = this.memberActiveServices[userId + '-' + serviceId];

        this.attendanceService.updateUserAttendance(userId, this.attendanceData['structure_entry_id'], serviceId, state)
            .subscribe((res) => res);
    }
}
